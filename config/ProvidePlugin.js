module.exports = {
  React: "react",
  Parse: "parse",
  connect: ["react-redux", "connect"],
  withRouter: ["react-router-dom", "withRouter"],
  Link: ["react-router-dom", "Link"]
};
