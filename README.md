# React easy template 😃

Start kit template React đã cấu hình cho dự án SPA.
-    `firebase` để đăng nhập với Facebook
-    `Parse` để lấy truy vấn dữ liệu
-    `redux` để quản lý state
-   `redux-saga` để xử lý bất đồng bộ với redux
-    `react-router-dom` v4 để xây dựng router
-    `react-router-redux@next` thêm midleware router vào redux
-    `history` quản lý lịch sử khi dùng với react-router-redux
-    `antd` làm giao diện ui
-    font chính `Comfortaa`
-    và một số package khác ...

## 🎓 Hướng dẫn

- Chỉnh sửa cấu hình Parse Server , Firebase SDK tại

```bash
 ./scr/redux/config.js
```

## 🚀 Bắt đầu dev

```bash
$ yarn
$ yarn start
```

## 🌀 Build dự án
```bash
$ yarn build
```

# 📌 Lưu ý 

+ Đã cấu hình `React`, `Parse` là các package toàn cục, không cần `import` vào các components nữa. 
Tham khảo tại đây [Webpack ProvidePlugin](https://redux-saga.js.org/)

## Webpack Alias :
- `@` : là thư mục `/scr`
- `*` : là thư mục `/scr/pages`
- `#` : là thư mục `/scr/components`
- `store` : là thư mục `/scr/redux`


## 👓 Tham khảo thêm

- [antd](http://github.com/ant-design/ant-design/)
- [babel-plugin-import](http://github.com/ant-design/babel-plugin-import/)
- [less-loader](https://github.com/webpack/less-loader)
- [firebase](https://firebase.google.com/docs/web/setup)
- [redux](http://redux.js.org/docs/introduction/)
- [redux-saga](https://redux-saga.js.org/)
- [animate.css](https://daneden.github.io/animate.css/)

# 🍀 CuDuy197 ™
