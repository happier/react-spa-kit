import { Route, Switch, Redirect } from "react-router-dom";

//page
import HomePage from "@/pages/HomePage";
import LoginPage from "./pages/LoginPage";
import NotFound from "./pages/NotFound";

//Ui
import TopNav from "./components/TopNav";

class AppContent extends React.Component {
  render() {
    const loggedIn = this.props.store.user.loggedIn;
    return (
      <div>
        <div>
          <TopNav />
          <Switch>
            <Route
              exact
              path="/"
              render={() =>
                loggedIn ? <HomePage /> : <Redirect to="/login" />}
            />
            <Route
              path="/login"
              render={() => (!loggedIn ? <LoginPage /> : <Redirect to="/" />)}
            />
            <Route component={NotFound} />
          </Switch>
        </div>
      </div>
    );
  }
}

export default withRouter(connect(store => ({ store }))(AppContent));
