import store from "store";
import * as userActions from "../redux/actions/user";
import { Form, Icon, Input, Button, notification, message } from "antd";
const FormItem = Form.Item;

class NormalLoginForm extends React.Component {
  state = {
    loadingLogin: false
  };
  handleSubmit = e => {
    this.setState({ loadingLogin: true });
    const hide = message.loading("Đang đăng nhập ...", 0);

    e.preventDefault();
    this.props.form.validateFields((err, input) => {
      if (!err) {
        let { userName, password } = input;

        Parse.User
          .logIn(userName, password)
          .then(userLogin => {
            this.setState({ loadingLogin: false });
            console.log("Parse Login Ok");
            let payload = {
              name: userLogin.get("username")
            };
            setTimeout(hide, 100);
            store.dispatch(userActions.login_with_pass_result(payload));

            notification["success"]({
              message: "Đăng nhập thành công!",
              description: `Chào mừng : ${userLogin.get("username")}`
            });
          })
          .catch(e => {
            setTimeout(hide, 250);
            message.error(`Có lỗi: ${e.message}`);
            console.log(e.message);
            this.setState({ loadingLogin: false });
          });
      } else {
        setTimeout(hide, 250);
        message.error(`Bạn hãy nhập đủ thông tin để đăng nhập.`);
        this.setState({ loadingLogin: false });
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        <FormItem>
          {getFieldDecorator("userName", {
            rules: [{ required: true, message: "Vui lòng nhập tài khoản!" }]
          })(
            <Input
              prefix={<Icon type="user" style={{ fontSize: 13 }} />}
              placeholder="Tài Khoản"
            />
          )}
        </FormItem>
        <FormItem>
          {getFieldDecorator("password", {
            rules: [{ required: true, message: "Vui lòng nhập mật khẩu!" }]
          })(
            <Input
              prefix={<Icon type="lock" style={{ fontSize: 13 }} />}
              type="password"
              placeholder="Mật khẩu"
            />
          )}
        </FormItem>
        <FormItem>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
            loading={this.state.loadingLogin}
          >
            <Icon type="login" /> Đăng nhập
          </Button>
        </FormItem>
      </Form>
    );
  }
}

const LoginForm = Form.create()(NormalLoginForm);
export default LoginForm;
