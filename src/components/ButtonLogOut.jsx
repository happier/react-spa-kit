import * as firebase from "firebase/app";
import "firebase/auth";
import { Modal, Button } from "antd";

class ButtonLogOut extends React.Component {
  logOut = () => {
    Modal.confirm({
      title: "Bạn có muốn đăng xuất ? ",
      onOk() {
        Parse.User
          .logOut()
          .then(() => {
            return firebase.auth().signOut();
          })
          .then(() => {
            console.log("LOGOUT");
            document.body.innerHTML =
              "<div style='text-align:center; padding-top: 12em;' class='animated infinite flash'> <h1 >Đang tải lại trang ...</h1> </div>";
            setTimeout(() => {
              window.history.go(0);
            }, 700);
          })
          .catch(function(error) {
            alert("Có lỗi! Hãy thử lại sau");
            console.log(error);
          });
      }
    });
  };
  render() {
    return (
      <Button icon="disconnect" onClick={this.logOut}>
        Đăng xuất
      </Button>
    );
  }
}

export default ButtonLogOut;
