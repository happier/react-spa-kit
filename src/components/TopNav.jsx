import { Menu, Icon } from "antd";
import { Link } from "react-router-dom";

class TopNav extends React.Component {
  render() {
    return (
      <div style={{ paddingBottom: "1em" }}>
        <Menu mode="horizontal">
          <Menu.Item key="home">
            <Link to="/">
              <Icon type="home" />Trang chủ
            </Link>
          </Menu.Item>
          <Menu.Item key="guide" style={{ float: "right" }}>
            <Link to="/guide">
              Trợ giúp <Icon type="question-circle-o" />
            </Link>
          </Menu.Item>
        </Menu>
      </div>
    );
  }
}

export default TopNav;
