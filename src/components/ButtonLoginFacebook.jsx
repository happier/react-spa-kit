import { Button, Icon, message } from "antd";
import * as firebase from "firebase/app";
import "firebase/auth";

class ButtonLoginFacebook extends React.Component {
  state = {
    loadingFacebookLogin: false
  };
  LoginFacebook = () => {
    message.loading("Đang đăng nhập bằng Facebook ...");
    this.setState({ loadingFacebookLogin: true });
    firebase
      .auth()
      .signInWithRedirect(new firebase.auth.FacebookAuthProvider());
  };

  render() {
    return (
      <Button
        onClick={this.LoginFacebook}
        loading={this.state.loadingFacebookLogin}
        size="large"
        style={{
          backgroundColor: "#3b5999",
          color: "white",
          border: "1px solid #3b5999"
        }}
      >
        <Icon type="global" /> Đăng nhập bằng Facebook
      </Button>
    );
  }
}

export default ButtonLoginFacebook;
