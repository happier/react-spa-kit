import { LocaleProvider } from "antd";
import viVN from "antd/lib/locale-provider/vi_VN";
import { Provider } from "react-redux";
//Store
import store, { history } from "store";
import { ConnectedRouter } from "react-router-redux";
//UI
import AppRouter from "./AppRouter";
import "./css/App.css";

class App extends React.Component {
  render() {
    return (
      <LocaleProvider locale={viVN}>
        <Provider store={store}>
          <ConnectedRouter history={history}>
            <AppRouter />
          </ConnectedRouter>
        </Provider>
      </LocaleProvider>
    );
  }
}

export default App;
