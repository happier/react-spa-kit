import { Spin } from "antd";
import LoginForm from "../components/LoginForm";
import ButtonLoginFacebook from "../components/ButtonLoginFacebook";

class Login extends React.Component {
  render() {
    const loggedIn = this.props.store.user.loggedIn;

    return (
      <div>
        {loggedIn === null
          ? <div style={{ paddingTop: "4em", textAlign: "center" }}>
              <Spin tip="Đang tải dữ liệu ..." size="large" />
            </div>
          : <div className="center login animated fadeIn">
              <h1>
                Chào mừng bạn tới trang Thử nghiệm Happier{" "}
                <span role="img" aria-labelledby="four_leaf">
                  🍀
                </span>{" "}
              </h1>
              <br />
              <ButtonLoginFacebook />
              <LoginForm />
            </div>}
      </div>
    );
  }
}

export default withRouter(connect(store => ({ store }))(Login));
