/* import { Button } from "antd"; */

import ButtonLogOut from "../components/ButtonLogOut";

class HomePage extends React.Component {
  render() {
    let userData = this.props.userData;
    return (
      <div>
        <div className="center animated fadeIn">
          <h1>
            Xin chào : <u style={{ color: "purple" }}>{userData.name} </u>{" "}
          </h1>
          <h1>
            {" "}Chúc bạn một ngày tốt lành{" "}
            <span style={{ color: "green" }} role="img" aria-label="lucky">
              🍀
            </span>
          </h1>
          <br /> <br /> <br />
          <ButtonLogOut />
        </div>
      </div>
    );
  }
}

export default withRouter(
  connect(store => ({
    store,
    userData: store.user
  }))(HomePage)
);
