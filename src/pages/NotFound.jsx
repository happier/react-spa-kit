import { Push } from "store";

class NotFound extends React.Component {
  componentDidMount() {
    setTimeout(function() {
      Push("/");
    }, 2000);
  }

  render() {
    return (
      <div className="center animated fadeInUp">
        <h1 style={{ paddingTop: "25vh" }}> Trang không tồn tại ! </h1>
        <br />
        <h3 className="center animated infinite flash">
          {" "}Trở về trang chủ ...{" "}
        </h3>
      </div>
    );
  }
}

export default NotFound;
