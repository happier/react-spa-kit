import * as types from "../types";

export const login_firebase_fb_result = payload => ({
  type: types.USER_LOGIN_FIREBASE_FB_RESULT,
  payload
});

export const login_with_pass_result = payload => ({
  type: types.USER_LOGIN_WITH_PASS_RESULT,
  payload
});

export const not_login = () => ({
  type: types.USER_NOT_LOGIN
});
