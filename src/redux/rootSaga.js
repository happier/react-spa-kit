import { all, take } from "redux-saga/effects";

import { watch_AUTH_CHANGED } from "./sagas/user";

function* logActions() {
	while (true) {
		const action = yield take("*"); // correct
		console.log(action);
	}
}

export default function* rootSaga() {
	yield all([watch_AUTH_CHANGED(), logActions()]);
}
