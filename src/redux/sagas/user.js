import store from "store";
import * as userActions from "../actions/user";
import * as firebase from "firebase/app";
import "firebase/auth";
import { configFirebase, ParseAppID, ParseServerURL } from "../config";
import { notification, message } from "antd";
//import { delay } from "redux-saga";
//import { put, call } from "redux-saga/effects";;

export function watch_AUTH_CHANGED() {
  //Firebase , Parse Init
  firebase.initializeApp(configFirebase);
  Parse.initialize(ParseAppID);
  Parse.serverURL = ParseServerURL;

  //Firebase
  firebase.auth().onAuthStateChanged(userFirebase => {
    if (userFirebase) {
      console.log("Xin chào: " + userFirebase.displayName);
      notification["success"]({
        message: `Đăng nhập thành công`
      });

      let payload = {
        name: userFirebase.displayName,
        email: userFirebase.email,
        uid: userFirebase.uid,
        id: userFirebase.providerData[0].uid
        /* photoUrl: userFirebase.providerData[0].photoURL */
      };

      if (Parse.User.current() !== null) {
        store.dispatch(userActions.login_firebase_fb_result(payload));
      } else {
        Parse.User
          .logIn(payload.id, payload.uid)
          .then(userParseLogin => {
            store.dispatch(userActions.login_with_pass_result(payload));
          })
          .catch(e => {
            //Register new user

            var ParseNewUser = new Parse.User();
            ParseNewUser.set("username", payload.id);
            ParseNewUser.set("name", payload.name);
            ParseNewUser.set("password", payload.uid);
            ParseNewUser.set("uid", payload.uid);
            ParseNewUser.set("email", payload.email);

            ParseNewUser.signUp()
              .then(newUser => {
                notification["success"]({
                  message: "Đã tạo tài khoản thành công!",
                  description: `Chào mừng : ${newUser.get("username")}`
                });
                newUser.setACL(new Parse.ACL(Parse.User.current()));
                newUser.save();
                store.dispatch(userActions.login_firebase_fb_result(payload));
              })
              .catch(e => {
                alert("Có lỗi : " + e.message);
                firebase.auth().signOut();
                document.body.innerHTML = `<div style='text-align:center; padding-top: 12em;' class='animated infinite flash'> <h1 >Có lỗi , đang tải lại trang ...</h1> </div>`;
                setTimeout(function() {
                  window.history.go(0);
                }, 500);
              });

            /*  */
          });
      }
    } else {
      console.log(
        "%c Bạn chưa đăng nhập firebase facebook...",
        "background: blue; color: white"
      );

      if (Parse.User.current() !== null) {
        console.log(
          "%c Bạn đã đăng nhập Parse ...",
          "background: lightgreen; color: white"
        );
        let payload = {
          name: Parse.User.current().get("name"),
          email: Parse.User.current().get("email"),
          id: Parse.User.current().get("username"),
          uid: Parse.User.current().get("uid")
        };
        store.dispatch(userActions.login_with_pass_result(payload));
      } else {
        console.log(
          "%c Bạn chưa đăng nhập Parse ...",
          "background: lightcoral; color: white"
        );

        message.warning("Vui lòng đăng nhập để tiếp tục.");
        store.dispatch(userActions.not_login());
      }
    }
  });
  console.log(
    "%c Kiểm tra đăng nhập ...! ",
    "background: purple; color: white"
  );

  //Debug
  /*   if (process.env.NODE_ENV === "development") {
    window.Parse = Parse;
    window.firebase = firebase;
  } */
}
