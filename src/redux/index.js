import { createStore, applyMiddleware } from "redux";
import createHistory from "history/createBrowserHistory";
import createSagaMiddleware from "redux-saga";
import { routerMiddleware, push } from "react-router-redux";

import rootReducer from "./rootReducer";
import rootSaga from "./rootSaga";

//MIDDLEWARE
const sagaMiddleware = createSagaMiddleware();
const history = createHistory();
const routerHistory = routerMiddleware(history);

// import  {push} from "react-router-redux";
function Push(andress) {
  store.dispatch(push(andress));
}
const store = createStore(
  rootReducer,
  applyMiddleware(routerHistory, sagaMiddleware)
);
//SAGA
sagaMiddleware.run(rootSaga);

export default store;
export { history, Push };

/* //Debug
if (process.env.NODE_ENV === "development") {
  window.store = store;
}
 */
