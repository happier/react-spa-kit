import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";

//Reducer
import user from "./reducers/user";

const rootReducer = combineReducers({
  user,
  router: routerReducer
});

export default rootReducer;
