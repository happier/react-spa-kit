import * as types from "../types";
/*import { Modal } from "antd";
const confirm = Modal.confirm;
 import * as firebase from "firebase/app";
import "firebase/auth";
import Parse from "parse"; */
//import { message, notification } from 'antd';

const userState = {
  loggedIn: null
};

const user = (state = userState, action) => {
  switch (action.type) {
    case types.USER_LOGIN_FIREBASE_FB_RESULT:
      state = action.payload;
      return { ...state, loggedIn: true };

    case types.USER_LOGIN_WITH_PASS_RESULT:
      state = action.payload;
      return { ...state, loggedIn: true };

    case types.USER_NOT_LOGIN:
      return { ...state, loggedIn: false };

    default:
      return state;
  }
};

export default user;
